Type this to start.
Ghost Talk, by Naej Doree:

<5AF7FF>"How can you not remember it ?" <FFFFFF>asked her boyfriend.
<72FF7A>"I don't know. It just happens."<FFFFFF> she answered, then she frowned,
<72FF7A>"In fact I kinda remember this happening to me back when I was a child."
<72FF7A>"I had a lot of trouble with my mother about it."<FFFFFF> added Eva, reminiscing her childhood.
<FFFFFF>Actually, this happened pretty often during Eva's childhood :

Her mother asked her to do something while she was reading a book and Eva answered, <72FF7A>"don't worry, I got it"<FFFFFF> but then didn't get it.
She even didn't remember her mom asking anything, nor her responding.
After many occurrences her mom finally understood that she was responding automatically without really processing what was happening.
And she concluded by telling her to listen instead of simply hearing.
But such a vague order didn't help the young girl and she kept on doing it, and her mom kept on disputing her until she eventually moved out for her studies.

After listening to this story, Tristan, her boyfriend, asked her if she had any idea on how to solve the problem.
She said: <72FF7A>"I think it will work if you just get my full attention before asking me to do stuff."
<FFFFFF>So Tristan tried getting her attention the next time, asking her if she was listening, getting a <72FF7A>"yes of course"<FFFFFF>, and then asked her to do stuff.
Didn't work.
He tried getting her to look at him before answering. Eva got annoyed by it and just dismissed him.
Didn't work.
And Tristan was getting drained by that. Either he didn't get her attention or got straight up hostility instead.
They needed to find a new solution, together.

So at some point they sat down and Tristan exposed the situation, saying it was not working and even hurting him.
<72FF7A>"Maybe you could try some more, maybe I'll soften up"<FFFFFF>, she answered, not realising how much he tried since she didn't remember half of it.
<5AF7FF>"No, I seriously can't or I'll just get mad at you, and none of us wants to fight over this."
<72FF7A>"My mum tried fighting over it and it didn't get her very far anyway."
<5AF7FF>"Can't you just not do it, not answer?" <FFFFFF>asked Tristan, a bit annoyed by his girlfriend letting him find the solution alone.
<72FF7A>"I can't"<FFFFFF>, she answered defensively. <72FF7A>"I don't even realise when it happens !"
<FFFFFF>Tristan stared into the void, not giving new input.
<72FF7A>"Wait, maybe that's the thing"<FFFFFF>, said Eva. <72FF7A>"I need to try and actually know when it happens, and act on it afterwards."
<FFFFFF>Tristan looked back at her.
At least this time the weight wasn't on his shoulders, but this seemed like a small step.
<5AF7FF>"Well, we can start with that"<FFFFFF>, he then said.
<FFFFFF>And they got back to their routine lives.

At first it didn't seem to change much but sometimes Eva, after agreeing to something, got back to him and asked about what he just said.
Then she began to do it more.
It still asked quite a bit of Tristan, who had to repeat himself often, but at least he wasn't receiving hostility.
And Eva realised more how much this impacted her life and the lives of people around her.
She progressively also realised more and more when she was ghost talking, and could get back more quickly to people to apologise for her inattention and ask what they said.
And that's where she stands now, getting better at recognising her problems and maybe, soon, reaching for some more help.

The end.

Thanks for playing.
The story will now loop so you can reach a higher score if you want to.