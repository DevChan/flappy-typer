﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

/// <summary>
/// Class used to spawn the obstacles
/// </summary>
public class Spawner : MonoBehaviour
{
    /// <value>The range of Y values the Spawner randomly used to spawn the object</value>
    public float randomRange;

    public int spawnCount = 0;

    [Serializable]
    public struct SpawnStep
    {
        public int start;
        public List<GameObject> toSpawn;
        public UnityEvent onStepStart;
    }

    public List<SpawnStep> steps;
    public int currentStep = 0;

    /// <summary>
    /// Instantiate the toSpawn object within [-randomRange,+randomRange] y values
    /// </summary>
    public void Spawn()
    {
        if (currentStep+1 < steps.Count && spawnCount >= steps[currentStep+1].start)
        {
            currentStep++;
        }

        List<GameObject> toSpawn = steps[currentStep].toSpawn;
        var current = Instantiate(toSpawn[Random.Range(0,toSpawn.Count)],transform);
        current.transform.position = new Vector3(current.transform.position.x,Random.Range(-randomRange, randomRange),current.transform.position.y);
        spawnCount++;

    }
}
