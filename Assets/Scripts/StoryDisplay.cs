﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StoryDisplay : MonoBehaviour
{
    public TextMeshProUGUI displayText;


    [Serializable]
    public struct characterColor
    {
        public string character;
        public Color color;
    }

    public List<characterColor> characterColors = new List<characterColor>();

    private Dictionary<int, Color> colorKeys = new Dictionary<int, Color>();
    

    public string extractColors(string text)
    {
        int keyPos;

        do
        {
            keyPos = text.IndexOf('<');
            if (keyPos < 0) break;
            //consume token

            var keyString = "";
            while (text[keyPos] != '>')
            {
                text = text.Remove(keyPos, 1);
                keyString += text[keyPos];
            }

            text = text.Remove(keyPos, 1);
            keyString = keyString.Remove(keyString.Length - 1);

            var color = Color.white;
            if (ColorUtility.TryParseHtmlString("#" + keyString, out color))
            {
                colorKeys.Add(keyPos, color);
            }
            else
            {
                Debug.Log("Could not parse color : " + keyString);
            }
        } while (keyPos >= 0);

        return text;
    }

    public void UpdateText(TypingManager typingManager)
    {
        int stringOffset = 0;
        Color lastColor = characterColors[0].color;
        foreach (var colorKey in colorKeys)
        {
            if (colorKey.Key <= typingManager.progress)
            {
                lastColor = colorKey.Value;
            }
        }

        string toDisplay = typingManager.ToType.Substring(typingManager.progress);
        toDisplay = openColor(lastColor) + toDisplay;
        stringOffset += openColor(lastColor).Length;

        foreach (var colorKey in colorKeys)
        {
            if (colorKey.Key > typingManager.progress)
            {
                string toInsert = closeColor(lastColor);
                lastColor = colorKey.Value;
                toInsert += openColor(lastColor);

                toDisplay = toDisplay.Insert(colorKey.Key - typingManager.progress + stringOffset, toInsert);
                stringOffset += toInsert.Length;
            }
        }

        toDisplay += closeColor(lastColor);
        displayText.text = toDisplay;
    }

    private string openColor(Color color)
    {
        return "<color=#" + ColorUtility.ToHtmlStringRGB(color) + ">";
    }

    private string closeColor(Color color)
    {
        return "</color>";
    }
}