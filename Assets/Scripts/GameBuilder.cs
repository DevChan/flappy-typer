﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

public static class GameBuilder
{
    #if UNITY_EDITOR

    [MenuItem("Build/Windows Build With Modding")]
    public static void BuildGame ()
    {
        // Get filename.
        string path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");
        string[] levels = new string[] {"Assets/Scenes/SampleScene.unity"};

        // Build player.
        BuildPipeline.BuildPlayer(levels, path + "/Game.exe", BuildTarget.StandaloneWindows, BuildOptions.None);

        // Copy a file from the project folder to the build folder, alongside the built game.
        FileUtil.DeleteFileOrDirectory(path + "/Readme.txt");
        FileUtil.CopyFileOrDirectory("Assets/Templates/Readme.txt", path + "/Readme.txt");

        Application.OpenURL(path);
        // Run the game (Process class from System.Diagnostics).
        /*
        Process proc = new Process();
        proc.StartInfo.FileName = path + "/BuiltGame.exe";
        proc.Start();
        */
    }
    #endif
}
