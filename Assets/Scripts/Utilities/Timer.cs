﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// A simple timer that calls an unity event when it ends.
/// </summary>
/// <remarks>
/// It should be attached to an active gameObject in order to work.
/// </remarks>
public class Timer : MonoBehaviour
{
    /// <value>Current time left</value>
    public float value = 0;
    /// <value>Tells if the timer is running or not</value>
    public bool running = false;
    /// <value>the time the timer will be reset to when calling ResetTimer.</value>
    public float duration = 1;
    /// <value>Allows you to make the timer resets and run again each time it ends.</value>
    public bool looping = false;
    /// <value>The event that will be raised when the timer reaches an end.</value>
    public UnityEvent onTimerEnd;
    
    // Update is called once per frame
    void Update()
    {
        if (value > 0 && running)
        {
            value -= Time.deltaTime;
        }
        else if (running)
        {
            ResetTimer();
            
            if (looping)
            {
                RunTimer();
            }
            onTimerEnd.Invoke();
        }
    }
    
    /// <summary>
    /// Used to stop and reset the timer.
    /// </summary>
    /// <remarks>
    /// It's called by default when the timer ends.
    /// </remarks>
    public void ResetTimer()
    {
        value = duration;
        running = false;
    }

    /// <summary>
    /// Used to run the timer.
    /// </summary>
    /// <remarks>
    /// It's called right after ResetTimer when the timer end and is looping.
    /// </remarks>
    public void RunTimer()
    {
        running = true;
    }

    public void SetDuration(float duration)
    {
        this.duration = duration;
        value = duration;
    }
}
