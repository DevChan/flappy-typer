﻿using System;
using DG.Tweening;
using UnityEngine;
/// <summary>
/// This script makes the gameObject able to flap
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class FlapMovement : MonoBehaviour
{
    private Rigidbody myRigidbody;

    /// <value>The speed and direction of the flap</value>
    public Vector3 flapForce;

    private int currentRotation;
    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }
    
    /// <summary>
    /// Make the gameObject flap
    /// </summary>
    public void Flap()
    {
        if(!enabled) return;
        myRigidbody.velocity = flapForce;

        currentRotation = (currentRotation + 90) % 360;

        transform.DOKill();
        transform.DOLocalRotate(new Vector3(0,currentRotation,0), 0.25f).SetEase(Ease.Linear);
    }

    public void FailedFlap()
    {
        if(!enabled) return;
        transform.DOLocalRotate(new Vector3(10, currentRotation, 10), 0.1f).OnComplete(() =>
            {
                transform.DOLocalRotate(new Vector3(-10, currentRotation, -10), 0.1f).OnComplete(() =>
                    {
                        transform.DOLocalRotate(new Vector3(0, currentRotation, 0), 0.1f);
                    });
            }
        );
    }

    public void OnDisable()
    {
        transform.DOKill();
    }

    public void OnLoop()
    {
        transform.DOKill();
        currentRotation = 0;
        transform.localRotation = Quaternion.Euler(0,0,0);
        
    }
}
