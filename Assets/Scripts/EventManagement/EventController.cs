﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Controller for all events from mouse/touchpad
/// </summary>
public class EventController : MonoBehaviour
{
    /// <value>The function called when an input is detected</value>
    public UnityEvent onAction;

    // Update is called once per frame
    public virtual void Update()
    {
        // Right mouseclick for pc
        if (Input.GetMouseButtonUp(0))
        {
            onAction.Invoke();
        }

        // Touch for mobile
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Ended)
            {
                onAction.Invoke();
            }

        }
    }
}
