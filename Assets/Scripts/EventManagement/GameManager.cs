﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// The Manager of the state of the game
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <value>The current score of the player</value>
    public int currentScore;
    
    /// <value>The text field used to display the score</value>
    public Text score;
    /// <value>The text field used to display the next characters to type</value>
    public StoryDisplay toType;
    /// <value>The text field used to display the end message</value>
    public Text endText;
    
    /// <summary>
    /// This enum describes a state of the game:
    /// waiting : when the play has not started yet
    /// running : the player is playing
    /// ended : the player lost
    /// </summary>
    public enum gameState
    {
        waiting,
        running,
        ended
    }

    /// <value>Callback invoked when the player decides to flap</value>
    public UnityEvent onFlap;
    
    /// <value>Callback invoked when the play start</value>
    public UnityEvent onStart;
    
    /// <value>Callback invoked when the player lost</value>
    public UnityEvent onEnd;
    
    /// <value>The current state of the game</value>
    public gameState state = gameState.waiting;

    /// <summary>
    /// Called when the player realizes an action
    /// </summary>
    public void onAction()
    {
        switch (state)
        {
            case gameState.waiting:
                StartGame();
                break;
            case gameState.running:
                Flap();
                break;
            case gameState.ended:
                break;
        }
    }

    /// <summary>
    /// Called when the player lost
    /// </summary>
    public void onGameEnd()
    {
        Time.timeScale = 1;
        state = gameState.ended;
        if (GetComponent<SaveSystemController>().addScore(currentScore))
        {
            endText.text = "NEW BEST SCORE : " + currentScore + " !!!";
            endText.fontStyle = FontStyle.Bold;
        }
        else
        {
            endText.text = "SCORE : " + currentScore + "  BEST SCORE : " + GetComponent<SaveSystemController>().getBestScore();
            //endText.fontStyle = FontStyle.Normal;
        }
        GetComponent<SaveSystemController>().Save();
        onEnd.Invoke();
    }

    /// <summary>
    /// Called when the player passes an obstacle
    /// </summary>
    /// <param name="enter">boolean saying if this is the beginning or the end of the collision with the reward area</param>
    /// <param name="reward">the reward area itself</param>
    public void onReward(bool enter, GameObject reward)
    {
        if (enter)
        {
            currentScore++;
            score.text = "SCORE: " + currentScore;
        }
    }
    /// <summary>
    /// Called when the play begins
    /// </summary>
    public void StartGame()
    {
        Time.timeScale = 1.3f;
        onStart.Invoke();
        state = gameState.running;
        Flap();
    }

    /// <summary>
    /// Called when the player flaps
    /// </summary>
    public void Flap()
    {
        onFlap.Invoke();
        UpdateTextToType();
    }
    
    /// <summary>
    /// Restarts the Scene
    /// </summary>
    public void RestartGame()
    {
        GetComponent<SaveSystemController>().Save();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void UpdateTextToType()
    {

        toType.UpdateText(GetComponent<TypingManager>());
    }

    public void ClearScreen()
    {
        foreach (Destroyable destroyable in FindObjectsOfType<Destroyable>())
        {
            destroyable.DestroyMe();
        }
    }

    public void ResetGame()
    {
        GetComponent<SaveSystemController>().ResetSave();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
