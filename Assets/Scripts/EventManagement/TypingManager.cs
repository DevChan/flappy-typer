﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;

public class TypingManager : EventController
{
    public bool isModdable = false;
    public TextAsset TextToType;

    public string ToType;

    public UnityEvent OnActionFailed;
    public UnityEvent OnRestartAction;
    public UnityEvent OnFinishText;
    public int progress = 0;

    private static string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwvxyz";
    // Start is called before the first frame update
    void Start()
    {
        progress = GetComponent<SaveSystemController>().getProgress();
        LoadText();
        FindObjectOfType<TextCube>().Init();
    }

    // Update is called once per frame
    public override void Update()
    {
        if (Input.GetKey(KeyCode.Backspace))
        {
            OnRestartAction.Invoke();
        }
        foreach (char c in Input.inputString)
        {
            if (String.Equals(c.ToString(), ToType[progress].ToString(), StringComparison.CurrentCultureIgnoreCase)
                || (!alphabet.Contains(ToType[progress]) && ToType[progress] != ' ' && !alphabet.Contains(c) && c != ' ')) 
                // punctuation can be filled by any non alphabetical character except spaces
            {
                progress++;
                onAction.Invoke();

                if (!alphabet.Contains(c))
                {
                    GetComponent<SaveSystemController>().updateProgress(progress);
                }
                
                if (ToType.Length == progress)
                {
                    progress = 0;
                    OnFinishText.Invoke();
                }

                
            }
            else
            {
                OnActionFailed.Invoke();
            }
        }
        
    }

    public void LoadText()
    {
        if (isModdable)
        {
            string path = Application.streamingAssetsPath + "/" + TextToType.name + ".txt";
            if (File.Exists(path))
            {
                ToType = "";
                StreamReader reader = new StreamReader(path);
                ToType += reader.ReadToEnd();
                reader.Close();
            }
            else
            {
                ToType = TextToType.name +
                         ".txt not found, please put a file named this way in the StreamingAssets folder.";
            }
        }
        else
        {
            ToType = TextToType.text;
        }
        ToType = ToType.Replace(System.Environment.NewLine, " ");
        ToType = Regex.Replace(ToType, @"\s+", " ");
        // parse the colors
        ToType = FindObjectOfType<StoryDisplay>().extractColors(ToType);
        progress = (progress + ToType.Length) % ToType.Length;
        if (ToType[progress] == ' ')
        {
            progress++;
        }
        
        GetComponent<GameManager>().UpdateTextToType();
    }

    public void Loop()
    {
        progress = 0;
        GetComponent<GameManager>().UpdateTextToType();
        FindObjectOfType<TextCube>().Init();
    }
}
