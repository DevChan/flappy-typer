﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextCube : MonoBehaviour
{
    public TextMeshPro[] texts;

    private int flapCount;

    private TypingManager _typingManager;
    // Start is called before the first frame update
    public void Init()
    {
        _typingManager = FindObjectOfType<TypingManager>();
        string current = (_typingManager.ToType+"    ").Substring(_typingManager.progress, 4);
        for (int i = 0; i < 4; i++)
        {
            texts[i].text = toCubeString(current[i]);
        }
    }

    public void OnFlap()
    {
        flapCount++;
        if (flapCount >= 4)
        {
            flapCount = 0;
        }
        UpdateBack();
    }

    public void UpdateBack()
    {
        string current = (_typingManager.ToType + "    ").Substring(_typingManager.progress, 4);
        texts[(flapCount+2)%4].text = toCubeString(current[2]);
    }

    private string toCubeString(char c)
    {
        if (c == ' ') return "[ ]";
        return c.ToString().ToUpper();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
