﻿using System.IO;
using UnityEngine;

/// <summary>
/// This class is used to save data and manage the saved data
/// </summary>
public class SaveSystemController : MonoBehaviour
{
    /// <value>The name of the file that will be created</value>
    public string saveName;
    private string savePath => Application.persistentDataPath + "/" + saveName + ".json";
    private Save save = new Save();
    
    void Awake()
    {
        LoadSave();
    }
    
    /// <summary>
    /// The function used to write down the save data to the hard drive
    /// </summary>
    public void Save()
    {
        FileStream file = File.Create(savePath);
        StreamWriter writer = new StreamWriter(file);
        string json = JsonUtility.ToJson(save,true);
        writer.Write(json);
        writer.Close();
        file.Close();
    }

    /// <summary>
    /// Loads the save data from the hard drive
    /// </summary>
    public void LoadSave()
    {
        if (File.Exists(savePath))
        {
            string json = "";
            StreamReader reader = new StreamReader(savePath);
            json += reader.ReadToEnd();
            reader.Close();
            save = JsonUtility.FromJson<Save>(json);
        }
    }

    /// <summary>
    /// Opens up a file explorer at the location of the save file
    /// </summary>
    [ContextMenu("LocateSave")]
    public void LocateSave()
    {
        Application.OpenURL(Application.persistentDataPath);
    }

    /// <summary>
    /// Stores the stor if it's a new best score
    /// </summary>
    /// <param name="score">value of the score</param>
    /// <returns>true if it's a new best score</returns>
    public bool addScore(int score)
    {
        return save.addScore(score);
    }
    /// <summary>
    /// Returns the best score of the player
    /// </summary>
    /// <returns>the best score of the player</returns>
    public int getBestScore()
    {
        return save.bestScore;
    }

    public void updateProgress(int progress)
    {
        save.storyProgress = progress;
    }

    public int getProgress()
    {
        return save.storyProgress;
    }

    public void ResetSave()
    {
        save = new Save();
        Save();
    }
}
