﻿using UnityEngine;
using System;

/// <summary>
/// The data structure that holds the save
/// </summary>
[Serializable]
public class Save
{
    /// <value>The best score of the player</value>
    [SerializeField] public int bestScore;
    
    /// <value>The current story progress of the player</value>
    [SerializeField] public int storyProgress;
    

    /// <summary>
    /// Saves the score if it's a new best score
    /// </summary>
    /// <param name="score"> the value of the score</param>
    /// <returns>true if it's a new best score</returns>
    public bool addScore(int score)
    {
        if (score >= bestScore)
        {
            bestScore = score;
            return true;
        }

        return false;
    }
}
